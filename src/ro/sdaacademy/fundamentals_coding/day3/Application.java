package ro.sdaacademy.fundamentals_coding.day3;

/*
Write a program that models a tiny Library
This particular library is composed of 8 numbered bookshelves. Each bookshelf holds books
that have their titles starting within a letter in set of three letters in alphabetical order
(eg bookshelf 1 holds book titles starting with letter a or b or c) except for the last one which
 holds the rest of the letters. Each bookshelf is composed of 5 shelves that can hold 2 books.
You program should be able to find books we are looking for by title or by author. It
    should tell you if the book exists in the library, and if so where.
You should be able to check-out a book and return it. You shouldn't be able to checkout a
    book that has already been checked out.
You should be able to add or remove books from the library. You shouldn't be able to remove books
    from the library is it is currently checked out.

You can use the list in books file to initialize your library;

 */
public class Application {
    public static void main(String[] args) {

    }
}
